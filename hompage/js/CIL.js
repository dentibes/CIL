
$(document).ready(function(){
	//메뉴 애니메이션
	var selectedMenu = "";
	$('.wrapHeader .header nav a').each(function(){
		if( $(this).hasClass('on') ) {
			selectedMenu = $(this).data("class");
		}
	});

	
	var headerSetTimeout;
	function mouseleave(){
		clearTimeout( headerSetTimeout );
		headerSetTimeout = setTimeout(function() {
			$('.wrapHeader .wrapSubMenu').attr("class", "wrapSubMenu " + selectedMenu);
		},  300);
	}


	$('.wrapHeader .header nav a').on({
		mouseenter: function(){
			clearTimeout( headerSetTimeout );
			$('.wrapHeader .wrapSubMenu').attr("class", "wrapSubMenu " + $(this).data("class"));
		}, mouseleave: mouseleave
	});

	$('.wrapHeader .wrapSubMenu ul').on({
		mouseenter: function(){
			clearTimeout( headerSetTimeout );
		}, mouseleave: mouseleave
	});

	$('.wrapHeader .wrapSubMenu').addClass( selectedMenu );


	//
});

function rotate() {
	var rotateSetTimeout;
	var rotateIndex = 0;
	$('.wrapRotate .rotateButton li').each(function(curIndex){
		if( $(this).hasClass('on') ) {
			rotateIndex = curIndex;
		}

		$(this).find('div').on({
			mouseenter: function(){
				clearTimeout( rotateSetTimeout );
				selectOn( curIndex );
			}, mouseleave: setTimer
		});
	});

	function setTimer(){
		clearTimeout( rotateSetTimeout );
		rotateSetTimeout = setInterval(function() {
			selectOn( rotateIndex%3 );
			rotateIndex += 1;
		},  2000);
	}

	function selectOn( index ){
		rotateIndex = index;
		$('.wrapRotate .rotateButton li').each(function(curIndex){
			if( index == curIndex ) {
				$(this).addClass('on');
				$('.wrapRotate section').eq( curIndex ).addClass('on');
			} else {
				$(this).removeClass('on');
				$('.wrapRotate section').eq( curIndex ).removeClass('on');
			}
		});
	}

	setTimer();
}